﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using Glass.Mapper.Sc.Web.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Netcore.Feature.MobileService.Serialization;
using Newtonsoft.Json.Linq;
using Sitecore;
using Sitecore.Abstractions;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.DependencyInjection;
using Sitecore.Layouts;
using Sitecore.Mvc.Common;
using Sitecore.Mvc.Devices;
using Sitecore.Mvc.Extensions;
using Sitecore.Mvc.Pipelines;
using Sitecore.Mvc.Pipelines.Response.GetPageRendering;
using Sitecore.Mvc.Pipelines.Response.RenderRendering;
using Sitecore.Mvc.Presentation;
using MvcPageContext = Sitecore.Mvc.Presentation.PageContext;
using PageContext = Sitecore.Layouts.PageContext;

namespace Netcore.Feature.MobileService.Controllers
{
    public class FeedController : GlassController
    {
        protected BaseCorePipelineManager CorePipelineManager =
            ServiceLocator.ServiceProvider.GetService<BaseCorePipelineManager>();

        protected FieldRenderer InternalFieldRenderer;
        public string Jresult;

        public ActionResult GetDefaultRendering()
        {
            var layoutField = new LayoutField(ContextItem);
            var layoutDefinition = LayoutDefinition.Parse(layoutField.Value);
            var defaultDeviceId = "{FE5D7FDF-89C0-4D99-9AA3-B5FBD009C9F3}";
            var deviceDefinition = layoutDefinition.GetDevice(defaultDeviceId);

            InternalFieldRenderer = new FieldRenderer(CorePipelineManager);
            InternalFieldRenderer.DisableExperienceEditorRendering = true;

            ProcessLayout(deviceDefinition);
            return Content(Jresult);
        }

        protected void ProcessRenderings(IEnumerable<RenderingDefinition> renderings)
        {
            var renderingDefinitions = renderings as RenderingDefinition[] ?? renderings.ToArray();
            var array = new JArray();
            foreach (var renderingDefinition in renderingDefinitions)
            {
                var datasourceId = renderingDefinition.Datasource;
                if (!string.IsNullOrEmpty(datasourceId))
                {
                    var rendering = ContextItem.Database.GetItem(ID.Parse(renderingDefinition.ItemID));
                    var ds = ContextItem.Database.GetItem(ID.Parse(datasourceId), ContextItem.Language);
                    if (rendering != null && ds != null)
                    {
                        var oFields = SerializeFields(ds);
                        var oRendering = new JObject();
                        oRendering["Id"] = renderingDefinition.UniqueId;
                        oRendering["Name"] = rendering.DisplayName;
                        oRendering["Placeholder"] = renderingDefinition.Placeholder;
                        oRendering["DataSource"] = oFields;
                        array.Add(oRendering);
                    }
                }
            }
            Jresult = array.ToString();
        }

        private JObject SerializeFields(Item item)
        {
            item.Fields.ReadAll();
            var fields = item.Fields.Where(f => !f.Name.StartsWith("__"));
            var oFields = new JObject
            {
                ["Id"] = item.ID.ToString(),
                ["Name"] = item.DisplayName
            };
            foreach (var field in fields)
                switch (field.TypeKey)
                {
                    case "droplink":
                    case "droptree":
                        var fReferenceField = (ReferenceField) field;
                        if (fReferenceField.TargetItem != null)
                            oFields[field.Name] = SerializeFields(fReferenceField.TargetItem);
                        break;
                    case "multilist":
                    case "multilist with search":
                    case "multilist with dynamic source":
                    case "treelist":
                        var fMultilist = (MultilistField) field;
                        var array = new JArray();
                        foreach (var selectedItem in fMultilist.GetItems())
                            array.Add(SerializeFields(selectedItem));
                        oFields[field.Name] = array;
                        break;
                    default:
                        oFields[field.Name] = InternalFieldRenderer.RenderField(field);
                        break;
                }
            return oFields;
        }

        protected void ProcessLayout(DeviceDefinition device)
        {
            if (device?.Renderings == null)
                return;
            var renderingDefinitions = device.Renderings.Cast<RenderingDefinition>();
            ProcessRenderings(renderingDefinitions);
        }

        public ActionResult RenderDefault()
        {
            var defaultDeviceId = ID.Parse("{FE5D7FDF-89C0-4D99-9AA3-B5FBD009C9F3}");
            SwitchContext(defaultDeviceId);
            var deviceItem = ContextItem.Database.GetItem(defaultDeviceId);
            var availableRenderings = ContextItem.Visualization.GetRenderings(deviceItem, false);

            var renderings = GetUsedRenderings(availableRenderings);
            InternalFieldRenderer = new FieldRenderer(CorePipelineManager);
            InternalFieldRenderer.DisableExperienceEditorRendering = true;

            ProcessRenderings(renderings);
            return Content(Jresult);
        }

        protected void ProcessRenderings(IEnumerable<RenderingReference> renderings)
        {
            var renderingReferences = renderings as RenderingReference[] ?? renderings.ToArray();
            var array = new JArray();
            foreach (var renderingReference in renderingReferences)
            {
                var datasourceId = renderingReference.Settings.DataSource;
                if (!string.IsNullOrEmpty(datasourceId))
                {
                    var ds = ContextItem.Database.GetItem(ID.Parse(datasourceId), ContextItem.Language);
                    if (ds != null)
                    {
                        var oFields = SerializeFields(ds);
                        var oRendering = new JObject();
                        oRendering["Id"] = renderingReference.UniqueId;
                        oRendering["Name"] = renderingReference.RenderingItem.DisplayName;
                        oRendering["Placeholder"] = renderingReference.Placeholder;
                        oRendering["DataSource"] = oFields;
                        array.Add(oRendering);
                    }
                }
            }
            Jresult = array.ToString();
        }

        private RenderingReference[] GetUsedRenderings(RenderingReference[] renderings)
        {
            var list = new List<RenderingReference>();
            var storage = Context.Items["Netcore.Placeholder"] as List<string>;
            if (storage != null)
                foreach (var placeholderName in storage)
                    list.AddRange(renderings.Where(r => r.Placeholder.EqualsText(placeholderName)));
            return list.ToArray();
        }

        private void SwitchContext(ID defaultDeviceId)
        {
            var device = new Device(defaultDeviceId.Guid);
            device.SetPropertyValue(ContextItem.Database.GetItem(defaultDeviceId));

            var pageContext = new MvcPageContext
            {
                RequestContext = HttpContext.Request.RequestContext,
                Item = ContextItem,
                Device = device
            };

            ContextService.Get().Push(pageContext);
            try
            {
                Context.Items["Netcore.Placeholder"] = null; //clear
                PlaceholderContext.Exit(); // need to exit current placeholder to get correct path

                var pageDefinition = pageContext.PageDefinition;
                var getPageRenderingArgs = new GetPageRenderingArgs(pageDefinition);
                PipelineService.Get().RunPipeline("mvc.getPageRendering", getPageRenderingArgs);
                var rendering = getPageRenderingArgs.Result;

                using (var textWriter = new StringWriter())
                {
                    var renderRenderingArgs = new RenderRenderingArgs(rendering, textWriter);
                    PipelineService.Get().RunPipeline("mvc.renderRendering", renderRenderingArgs);
                }
            }
            finally
            {
                if (ContextService.Get().GetCurrentOrDefault<PageContext>() != null)
                    ContextService.Get().Pop<PageContext>();
            }
        }
    }
}