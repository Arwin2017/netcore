﻿using Sitecore.Abstractions;
using Sitecore.Collections;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Pipelines;
using Sitecore.Pipelines.RenderField;

namespace Netcore.Feature.MobileService.Serialization
{
    public class FieldRenderer : IFieldRenderer
    {
        protected BaseCorePipelineManager PipelineManager;

        public bool DisableExperienceEditorRendering { get; set; }

        public FieldRenderer(BaseCorePipelineManager pipelineManager)
        {
            Assert.ArgumentNotNull((object)pipelineManager, "pipelineManager");
            this.PipelineManager = pipelineManager;
        }

        public virtual string RenderField(Field field)
        {
            RenderFieldArgs renderFieldArgs1 = new RenderFieldArgs(this.DisableExperienceEditorRendering);
            renderFieldArgs1.After = string.Empty;
            renderFieldArgs1.Before = string.Empty;
            renderFieldArgs1.EnclosingTag = string.Empty;
            Item obj = field.Item;
            renderFieldArgs1.Item = obj;
            string name = field.Name;
            renderFieldArgs1.FieldName = name;
            string empty1 = string.Empty;
            renderFieldArgs1.Format = empty1;
            SafeDictionary<string> safeDictionary1 = new SafeDictionary<string>();
            renderFieldArgs1.Parameters = safeDictionary1;
            string empty2 = string.Empty;
            renderFieldArgs1.RawParameters = empty2;
            SafeDictionary<string> safeDictionary2 = new SafeDictionary<string>();
            renderFieldArgs1.RenderParameters = safeDictionary2;
            int num = 0;
            renderFieldArgs1.DisableWebEdit = num != 0;
            RenderFieldArgs renderFieldArgs2 = renderFieldArgs1;
            if (field.Item.Fields[field.Name].TypeKey == "multi-line text")
                renderFieldArgs2.RenderParameters["linebreaks"] = "<br/>";
            this.PipelineManager.Run("renderField", (PipelineArgs)renderFieldArgs2);
            return renderFieldArgs2.Result.ToString();
        }
    }
}
