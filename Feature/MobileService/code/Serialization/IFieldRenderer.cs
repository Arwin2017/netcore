﻿using Sitecore.Data.Fields;

namespace Netcore.Feature.MobileService.Serialization
{
    public interface IFieldRenderer
    {
        bool DisableExperienceEditorRendering { get; set; }

        string RenderField(Field field);
    }
}
