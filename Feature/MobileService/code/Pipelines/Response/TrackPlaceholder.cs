﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sitecore;
using Sitecore.Diagnostics;
using Sitecore.Mvc.Extensions;
using Sitecore.Mvc.Pipelines.Response.RenderPlaceholder;
using Sitecore.Mvc.Presentation;

namespace Netcore.Feature.MobileService.Pipelines.Response
{
    public class TrackPlaceholder : RenderPlaceholderProcessor
    {
        public override void Process(RenderPlaceholderArgs args)
        {
            Assert.ArgumentNotNull(args, "args");
            Render(args.PlaceholderName, args);
        }

        protected virtual void Render(string placeholderName, RenderPlaceholderArgs args)
        {
            string placeholderPath = PlaceholderContext.Current.ValueOrDefault<PlaceholderContext, string>((Func<PlaceholderContext, string>)(context => context.PlaceholderPath)).OrEmpty();
            var storage = Context.Items["Netcore.Placeholder"] as List<string>;
            if (storage == null)
            {
                storage = new List<string>();
            }
            storage.Add(placeholderPath);
            Context.Items["Netcore.Placeholder"] = storage;
        }
    }
}